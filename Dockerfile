FROM  docker-images.jobtechdev.se/mirror/python:3.9.10-slim-bullseye
EXPOSE 8081

RUN apt-get update -y && \
    apt-get install -y git build-essential && \
    apt-get clean -y

COPY . /app

WORKDIR /app

RUN python3 -m pip install --upgrade setuptools wheel pip && \
    pip3 install -r requirements.txt

CMD  uwsgi --http :8081 --manage-script-name --mount /=app:app
