# Aardvark-demo

Purpose of this repository is to have a very simple demo project to
demonstrate how to use the Aardvark build and deploy pipeline.

This is the most simple Flask app to be run in a container. To build and launch:
```shell
podman build -t aardvark-demo:latest .
podman run -p 8081:8081 --rm --name=demo aardvark-demo:latest
```

